<?php
/**
 * Deletes old registries from our gitlab that do not have a corresponding branch to it
 */

$gitlab_url = getenv("GITLAB_URL");
$token = getenv("TOKEN");
$projects = getProjects();

print("Checking for registry images that belong to non existing branches!" . PHP_EOL);

foreach ($projects as $index => $project) {
    print("[" . ($index + 1) . "/" . sizeof($projects) . "] Checking project " . $project["name_with_namespace"] . PHP_EOL);

    $registries = getRegistry($project["id"]);
    foreach ($registries as $index => $registry) {
        $branch = getBranch($registry["project_id"], $registry["name"]);
        if (empty($branch)) {
            // Branch existiert nicht
            print("\t[" . ($index + 1) . "/" . sizeof($registries) . "] Branch " . $registry["name"] . " existiert nicht! Removing registry entry... ");
            deleteRegistryEntry($project["id"], $registry["id"]);
            print(PHP_EOL);
        } else {
            print("\t[" . ($index + 1) . "/" . sizeof($registries) . "] Branch " . $registry["name"] . " existiert! Removing all tags but the latest... ");
            deleteRegistryTags($project["id"], $registry["id"]);
            print(PHP_EOL);

        }
    }
}

function deleteRegistryTags($projectId, $registryId)
{
    global $token, $gitlab_url;
    $url = $gitlab_url . "/api/v4/projects/$projectId/registry/repositories/$registryId/tags?private_token=$token&keep_n=1&name_regex=.*";
    $ch = curl_init();
    try {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        print($httpCode);
    } finally {
        curl_close($ch);
    }
}

function deleteRegistryEntry($projectId, $registryId)
{
    global $token, $gitlab_url;
    $url = $gitlab_url . "/api/v4/projects/$projectId/registry/repositories/$registryId?private_token=$token";
    $ch = curl_init();
    try {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        print($httpCode);
    } finally {
        curl_close($ch);
    }
}

function getBranch($projectId, $branchName)
{
    global $token, $gitlab_url;
    $branch = file_get_contents($gitlab_url . "/api/v4/projects/$projectId/repository/branches?private_token=$token" . "&search=^" . urlencode($branchName));
    $branch = json_decode($branch);
    return $branch;
}

function getRegistry($id)
{
    global $token, $gitlab_url;
    $resultregistries = [];
    $page = 1;

    while (true) {
        $registries = file_get_contents($gitlab_url . "/api/v4/projects/$id/registry/repositories?private_token=$token" . "&page=$page");
        $registries = json_decode($registries, true);
        $resultregistries = array_merge($resultregistries, $registries);

        $nextPage;
        foreach ($http_response_header as $header) {
            if (stripos($header, "X-Next-Page") !== false) {
                $nextPage = trim(explode(":", $header)[1]);
            }
        }
        if (empty($nextPage)) {
            break;
        } else {
            $page = $nextPage;
        }
    }
    return $resultregistries;
}

function getProjects()
{
    global $token, $gitlab_url;
    $resultProjects = [];
    $page = 1;

    while (true) {
        $projects = file_get_contents($gitlab_url . "/api/v4/projects?simple=true&private_token=$token" . "&page=$page");
        $projects = json_decode($projects, true);
        $resultProjects = array_merge($resultProjects, $projects);

        $nextPage;
        foreach ($http_response_header as $header) {
            if (stripos($header, "X-Next-Page") !== false) {
                $nextPage = trim(explode(":", $header)[1]);
            }
        }
        if (empty($nextPage)) {
            break;
        } else {
            $page = $nextPage;
        }
    }
    return $resultProjects;
}
