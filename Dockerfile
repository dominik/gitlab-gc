FROM dtzar/helm-kubectl

RUN apk add --update \
    curl \
    jq \
    php7-cli \
    php7-curl \
    php7-json \
    php7-openssl \
    && rm -rf /var/cache/apk

COPY scripts /scripts
WORKDIR /scripts

CMD php deleteOldBranches.php